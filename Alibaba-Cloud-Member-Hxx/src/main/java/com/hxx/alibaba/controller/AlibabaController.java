package com.hxx.alibaba.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author huangxiangxiang
 * @version 2.0.0
 * @createTime 2019年08月19日 17:09:00
 */

@RestController
@RefreshScope
public class AlibabaController {


    @Value("${didispace.title:}")
    private String textName;

    @Value("${server.port}")
    private String ServerPort;


    @GetMapping("/getName")
    public String getName() {
        System.out.println("测试............" + textName + ": " + ServerPort);
        return textName;
    }
}
